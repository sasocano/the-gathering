let nameHeading = document.getElementById("inpName");
let divMain = document.getElementById("divMain");
let inp = document.getElementById("search")
let selectColor = document.getElementById("select")
let selectSort = document.getElementById("sorting")
let selectType = document.getElementById("types")
let name = localStorage.newName;
let loadContent = true
let showHide = document.getElementById("showHide")
nameHeading.innerText = "Welcome " + name;

let allCards = [];

// showing loading spinner before data is loaded
loadContent ? spinner.removeAttribute('hidden') : null

//fetching data for the cards
fetch("https://api.magicthegathering.io/v1/cards?random=true&pageSize=100&language=English")
    .then(res => res.json())
    .then(data => {
        allCards = [...data.cards]
        cards(allCards)
        loadContent = false
        spinner.setAttribute('hidden', '');
    })

//fetching data for the types of the card
fetch("https://api.magicthegathering.io/v1/types")
    .then(res => res.json())
    .then(data => {
        let typeArr = data.types.slice()
        typeArr.forEach(el => {
            selectType.innerHTML += `<option value="${el}">${el}</option> `
        })
    })

//function for creating card layout and card info
cards = (obj) => {
    obj.forEach(el => {
        divMain.innerHTML +=
            `<div class="card morph">
                <div class="divText">
                    <p class="para">Name:</p>
                    <h2>${el.artist}</h2>
                    <p class="para">Type:</p>
                    <h3>${el.types}</h3>
                    <p class="para">Name set:</p>
                    <h4>${el.name}</h4>
                    <p class="para">Color:</p>
                    <h5>${el.colors}</h5>
                </div>
                <div class="divCardText">
                    <p class="para">Card description</p>
                    <p class="paraText">${el.text}</p>
                </div>
                <div>
                <img class="img" src="${el.imageUrl}"/>
                </div>
            </div>`
    })
}

//function for searching cards by name(artist) or card text from input value
inp.addEventListener("keyup", (e) => {
    let searchString = e.target.value
    let filteredCards = allCards.filter(el => el.text && el.text.toLowerCase().indexOf(searchString.toLowerCase()) > -1 || el.artist.toLowerCase().indexOf(searchString.toLowerCase()) > -1)
    divMain.innerHTML = " "
    cards(filteredCards)
})

//function for showing cards by color
selectColor.addEventListener("change", () => {
    var value = selectColor.options[selectColor.selectedIndex].value;
    let filterCard = allCards.filter(el => el.colors.includes(value))
    divMain.innerHTML = " "
    value === "All" ? cards(allCards) : cards(filterCard)
})

//function for showing cards by type
selectType.addEventListener("change", (e) => {
    let value = e.target.value
    let filterCard = allCards.filter(el => el.types.includes(value))
    divMain.innerHTML = " "
    value === "All" ? cards(allCards) : cards(filterCard)
})

//function for showing cards by ascending or descending order
selectSort.addEventListener("change", () => {
    let newArr = [...allCards]
    var value = selectSort.options[selectSort.selectedIndex].value;
    divMain.innerHTML = " "

    newArr.sort((a, b) => {
        let nameA = a.artist.toLowerCase()
        let nameB = b.artist.toLowerCase()

        if (value === "All") {
            cards(allCards)
        }

        if (value === "Ascending") {
            if (nameA < nameB) {
                return -1;
            }
        } else if (value === "Descending") {
            if (nameA > nameB) {
                return -1
            }
        }
        return 0
    })
    cards(newArr)
})